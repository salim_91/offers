-- MySQL dump 10.13  Distrib 5.7.18, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: springtutorial
-- ------------------------------------------------------
-- Server version	5.7.18-enterprise-commercial-advanced-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_messages_users1_idx` (`username`),
  CONSTRAINT `fk_messages_users1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,'Test Mail','Testin messae','Salim','testuser@mail.in','testuser'),(4,'Teijdni','jsdvnksdjnvkjs dksdkj skdj sjkdfn skdf skdj','Salim','salim@wipro.com','fakeadmin'),(5,'it is a test messa','jsdnckjds  kjvnj kjvnskdj kjv knksdjnv skd vkdjvnk','testuser','testuser@mail.in','testuser'),(6,'Testin Refres of messaes','ello Test User, ow are you???\r\nLookinf forward for yr replu','testuser','testuser@mail.in','testuser'),(7,'Testin Refres of messaes','ello fake admin, ow are you doin, lookin forward for yr answer','testuser','testuser@mail.in','fakeadmin'),(8,'jakkjbas','kajkjascaskjnkjasc j kjasnkjcasn','testuser','testuser@mail.in','fakeadmin'),(9,'asdsadas','asdasasascasdasdasdascdcdcas','akeel','akeel4960@gmail.com','fakeadmin'),(10,'asdfsv f','dsfwjfdsnds nfs         ds fsfsf dsfffff             skvnsk','aadasd','ada@mail.com','fakeadmin'),(11,'dsfsdf','dfds                                                      dfssssssssssssssssss ','testuser','testuser@mail.in','testuser'),(12,'adfdf','sfv sdvvvvvvvvvvvvvsd vvvvvvvvvvsd','testuser','testuser@mail.in','fakeadmin'),(13,'mail.com','sjnksn jdsknjkns sjvnks','testuser','testuser@mail.in','testuser'),(14,'jdbfadjsfbadsj','kdafnkjsfksdfk dsfkndskfnkjds nsdkfjkdsnfkk','fakeadmin','fakeadmin@mail.in','testuser');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `username` varchar(60) NOT NULL,
  PRIMARY KEY (`id`,`username`),
  KEY `fk_offers_users_idx` (`username`),
  CONSTRAINT `fk_offers_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (22,'NEw offer is created aain .s.dad','testuser'),(23,'jdbfcjsdbjsd kjdnkjasn','fakeadmin');
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(60) NOT NULL,
  `password` varchar(80) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `email` varchar(60) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `authority` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('akeel123','16c50a2601f4c8ddbf9158f24942a8c817e8702ac82f8658e497d656c7d6b4139641244124a7dbf6',1,'akeel4960@gmail.com','akeel','ROLE_USER'),('fakeadmin','8875fcb717b6fda990f54aa5890de77ab4af2c43cc095769d6044f9244fea688d8198c35bcde1e1a',1,'fakeadmin@mail.in','fakeadmin','ROLE_USER'),('salim123','b31655bd849175c537798fc71b16d41c0f3b98970d4e8aec21221ccb05f9db5e4dad568f0cf4a139',1,'salim@hma.com','salim123','ROLE_USER'),('testuser','f8324c8be54e516cd4c07a3520e14b97cdaa4125379c781530e0df6ea49e96b244c5e5b098ed9c51',1,'testuser@mail.in','testuser','ROLE_USER');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'springtutorial'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-08 12:07:52
